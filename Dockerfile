FROM adoptopenjdk/openjdk16
VOLUME ["/home"]
ENTRYPOINT [ "java", "-jar", "-Dspring.profiles.active=release", "/home/spring-boot-webflux-retobcp-2-0.0.1-SNAPSHOT.jar"]
