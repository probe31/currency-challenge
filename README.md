# Desafío - Tipo de cambio

## Participante: Bhérring Hugo Paucar Bonifacio

Lista de endpoints:
* **[GET]**  /api/exchange/     : Lista todos los tipos de cambios disponibles (EUR, USD, PEN)
* **[POST]** /api/exchange/     : Aplicar tipo de cambio
* **[PUT]**  /api/exchange/{id} : (PREGUNTA OPCIONAL 2) Permite actualizar el tipo de cambio

## Aplicar tipo de cambio a un monto:
Ejemplo de JSON enviado:
```json
{
    "monto": 200,
    "monedaOrigen": "PEN",
    "monedaDestino": "USD"
}
```

Ejemplo de JSON recibido:
```json
{
    "monto": 200.0,
    "tipoDeCambio": 0.24,
    "monedaOrigen": "PEN",
    "montoConTipoDeCambio": 48.0,
    "monedaDestino": "USD"
}
```