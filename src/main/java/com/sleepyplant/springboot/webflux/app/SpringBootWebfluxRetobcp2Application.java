package com.sleepyplant.springboot.webflux.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import com.sleepyplant.springboot.webflux.app.models.documents.ExchangeRate;
import com.sleepyplant.springboot.webflux.app.models.services.ExchangeRateService;

import reactor.core.publisher.Flux;

@SpringBootApplication
public class SpringBootWebfluxRetobcp2Application implements CommandLineRunner{

	@Autowired
	private ExchangeRateService service;
	
	@Autowired
	private ReactiveMongoTemplate mongoTemplate;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebfluxRetobcp2Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		//generación de fake-data 		
		mongoTemplate.dropCollection("exchangeRates").subscribe();
		
		Flux.just(
				new ExchangeRate("USD", "EUR", 0.85),
				new ExchangeRate("USD", "PEN", 4.11),
				new ExchangeRate("PEN", "EUR", 0.21),
				new ExchangeRate("PEN", "USD", 0.24),
				new ExchangeRate("EUR", "PEN", 4.84),
				new ExchangeRate("EUR", "USD", 1.18)											
					)
			.flatMap(exchangeProduct -> {
				return service.save(exchangeProduct);
			}).subscribe();
					
						
				
		
	}

}
