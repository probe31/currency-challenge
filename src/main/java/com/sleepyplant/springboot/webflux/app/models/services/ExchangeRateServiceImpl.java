package com.sleepyplant.springboot.webflux.app.models.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import com.sleepyplant.springboot.webflux.app.models.dao.ExchangeRateDao;
import com.sleepyplant.springboot.webflux.app.models.documents.ExchangeRate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

	@Autowired 
	private ExchangeRateDao exchangeRateDao;
		
	@Override
	public Flux<ExchangeRate> findAll() {
		return exchangeRateDao.findAll();
	}

	@Override
	public Mono<ExchangeRate> findById(String id) {
		return exchangeRateDao.findById(id);
	}
	
	@Override
	public Mono<ExchangeRate> findByMonedaOrigenMonedaDestino(String monedaOrigen, String monedaDestino) {
		
		ExchangeRate exchangeRate = new ExchangeRate();
		exchangeRate.setMonedaDestino(monedaDestino.toUpperCase());
		exchangeRate.setMonedaOrigen(monedaOrigen.toUpperCase());		
		Example<ExchangeRate> match = Example.of(exchangeRate);		
		return exchangeRateDao.findOne(match);
	}	

	@Override
	public Mono<ExchangeRate> save(ExchangeRate exchangeRate) {
		return exchangeRateDao.save(exchangeRate);
	}

	@Override
	public Mono<Void> delete(ExchangeRate exchangeRate) {
		return exchangeRateDao.delete(exchangeRate);
	}
	

}
