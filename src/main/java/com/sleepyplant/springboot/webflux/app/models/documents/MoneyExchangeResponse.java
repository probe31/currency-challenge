package com.sleepyplant.springboot.webflux.app.models.documents;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class MoneyExchangeResponse {

	@NotNull
	private Double monto;
	@Valid	
	private ExchangeRate exchangeRate;	
	@NotNull
	private Double montoConTipoDeCambio;
	
	
	public MoneyExchangeResponse() {	
	}


	public MoneyExchangeResponse(Double amount, ExchangeRate exchangeRate, Double result) {	
		this.monto = amount;
		this.exchangeRate = exchangeRate;
		this.montoConTipoDeCambio = result;
	}


	public Double getMonto() {
		return monto;
	}


	public void setMonto(Double monto) {
		this.monto = monto;
	}


	public ExchangeRate getExchangeRate() {
		return exchangeRate;
	}


	public void setExchangeRate(ExchangeRate exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	public Double getMontoConTipoDeCambio() {
		return montoConTipoDeCambio;
	}


	public void setMontoConTipoDeCambio(Double montoConTipoDeCambio) {
		this.montoConTipoDeCambio = montoConTipoDeCambio;
	}


	
	
	
	
}
