package com.sleepyplant.springboot.webflux.app.models.documents;




import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "exchangeRates")
public class ExchangeRate {
	
	@Id
	private String id;
	
	@NotEmpty	
	private String monedaOrigen;
	
	@NotEmpty
	private String monedaDestino;
	
	@NotNull
	private Double tipoDeCambio;

	public ExchangeRate() {
		
	}

	public ExchangeRate(String monedaOrigen, String monedaDestino, Double tipoDeCambio) {		
		this.monedaOrigen = monedaOrigen;
		this.monedaDestino = monedaDestino;
		this.tipoDeCambio = tipoDeCambio;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	public Double getTipoDeCambio() {
		return tipoDeCambio;
	}

	public void setTipoDeCambio(Double tipoDeCambio) {
		this.tipoDeCambio = tipoDeCambio;
	}

	public String getId() {
		return id;
	}

		
		
	
}
