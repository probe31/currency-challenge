package com.sleepyplant.springboot.webflux.app.models.services;
import com.sleepyplant.springboot.webflux.app.models.documents.ExchangeRate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ExchangeRateService {

	public Flux<ExchangeRate> findAll();
	
	public Mono<ExchangeRate> findById(String id);
	
	public Mono<ExchangeRate> findByMonedaOrigenMonedaDestino(String monedaOrigen, String monedaDestino);
	
	public Mono<ExchangeRate> save(ExchangeRate exchangeRate);
	
	public Mono<Void> delete(ExchangeRate exchangeRate);
	
}
