package com.sleepyplant.springboot.webflux.app.validators;

import java.util.Currency;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MonedaValidator implements ConstraintValidator<Moneda, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		
		boolean result =false;
		if(value.length()==3)
			result = true;
		
		try {
			Currency currency = Currency.getInstance(value);			
			result = true;
		} catch (Exception e) {			
			result = false;
		}					
		return result;
	}

}
