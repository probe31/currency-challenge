package com.sleepyplant.springboot.webflux.app.models.documents;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.sleepyplant.springboot.webflux.app.validators.Moneda;

public class MoneyExchangeRequest {

	@NotNull(message = "Debe ingresar un valor decimal positivo")
	@DecimalMin(value = "0.0", inclusive = false, message = "Debe ingresar un valor decimal positivo")
	private Double monto;
		
	@NotEmpty(message = "Debe ingresar un valor tipo de cambio válido")
	@Moneda
	private String monedaOrigen;
	
	@NotEmpty(message = "Debe ingresar un valor tipo de cambio válido")
	@Moneda
	private String monedaDestino;
	
	public MoneyExchangeRequest() {	
	}

	public MoneyExchangeRequest(Double originalAmount, String monedaOrigen, String monedaDestino) {
		this.monto = originalAmount;
		this.monedaOrigen = monedaOrigen;
		this.monedaDestino = monedaDestino;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	
}
