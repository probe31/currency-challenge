package com.sleepyplant.springboot.webflux.app.controllers;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebExchangeBindException;
import com.sleepyplant.springboot.webflux.app.models.documents.ExchangeRate;
import com.sleepyplant.springboot.webflux.app.models.documents.MoneyExchangeRequest;
import com.sleepyplant.springboot.webflux.app.models.services.ExchangeRateService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/api/exchange")
public class MoneyExchangeController {

	@Autowired
	private ExchangeRateService service;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<ExchangeRate>>> lista(){
		return Mono.just(
				ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(service.findAll()) 
				);
	}
	
	@PostMapping
	public Mono<ResponseEntity<Map<String,Object>>> exchange(@Valid @RequestBody MoneyExchangeRequest moneyExchangeRequest){
		
		Map<String, Object> respuesta = new HashMap<String, Object>();
			
		return service.findByMonedaOrigenMonedaDestino(
				moneyExchangeRequest.getMonedaOrigen(), 
				moneyExchangeRequest.getMonedaDestino()).map(me->{
			
			respuesta.put("monto", moneyExchangeRequest.getMonto());
			respuesta.put("tipoDeCambio", me.getTipoDeCambio());
			respuesta.put("montoConTipoDeCambio", me.getTipoDeCambio()*moneyExchangeRequest.getMonto());
			respuesta.put("monedaOrigen", me.getMonedaOrigen());
			respuesta.put("monedaDestino", me.getMonedaDestino());
			
			return ResponseEntity
					.created(URI.create("/api/exchange/result"))
					.contentType(MediaType.APPLICATION_JSON)
					.body(respuesta);
			
		}).defaultIfEmpty(ResponseEntity.notFound().build());		
	
	}
	
	@PutMapping("/{id}")
	public Mono<ResponseEntity<ExchangeRate>> editar(@Valid @RequestBody ExchangeRate exchangeRate, @PathVariable String id){
		
		return service.findById(id).flatMap(p -> {
			p.setTipoDeCambio(exchangeRate.getTipoDeCambio());			
			return service.save(p);
		}).map(p->ResponseEntity
				.created(URI.create("/api/exchange/".concat(p.getId())))
				.body(p))
			.defaultIfEmpty(ResponseEntity.notFound().build());
		
	}
	
	@ControllerAdvice
	public class ValidationHandler {

	    @ExceptionHandler(WebExchangeBindException.class)
	    public ResponseEntity<List<String>> handleException(WebExchangeBindException e) {
	        var errors = e.getBindingResult()
	                .getAllErrors()
	                .stream()
	                .map(DefaultMessageSourceResolvable::getDefaultMessage)
	                .collect(Collectors.toList());
	        return ResponseEntity.badRequest().body(errors);
	    }

	}
		
}
