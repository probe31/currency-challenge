package com.sleepyplant.springboot.webflux.app.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MonedaValidator.class)
@Documented
public @interface Moneda {

	String message() default "{Tipo de moneda ingresado no existe}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
	  
}
