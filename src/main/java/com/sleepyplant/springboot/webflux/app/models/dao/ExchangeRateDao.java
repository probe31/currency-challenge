package com.sleepyplant.springboot.webflux.app.models.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import com.sleepyplant.springboot.webflux.app.models.documents.ExchangeRate;

public interface ExchangeRateDao extends ReactiveMongoRepository<ExchangeRate, String> {
		
}
